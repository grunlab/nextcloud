[![pipeline status](https://gitlab.com/grunlab/nextcloud/badges/main/pipeline.svg)](https://gitlab.com/grunlab/nextcloud/-/commits/main)

# GrunLab Nextcloud

Nextcloud (with supervisord) container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/nextcloud.md
- https://docs.grunlab.net/apps/nextcloud.md

Base image: [docker.io/nextcloud:apache][base-image]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this service:
- [grunlab/nextcloud][nextcloud]

[base-image]: <https://hub.docker.com/_/nextcloud>
[nextcloud]: <https://gitlab.com/grunlab/nextcloud>